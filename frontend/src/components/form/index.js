import { Form, Input } from 'antd';

export default function CatForm({handleOnFinished, handleOnError, ...rest}){
  const onFinish = (values) => {
      let key = Object.keys(values)[0]
      if(key==='tags'){
            values[key] = values[key].split(",").map(v=>v.trim())
      }
      handleOnFinished(values)
  }

  const onFinishFailed = (errorInfo) => {
    handleOnError(errorInfo)
  }

  return (
    <Form
      name="basic"
      labelCol={{ span: 3 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      onValuesChange={onFinish}
      {...rest}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ message: 'Please input their name' }]}
      >
        <Input placeholder="Millie"/>
      </Form.Item>
      <Form.Item
        label="Color"
        name="color"
        rules={[{ message: 'Please input their color' }]}
      >
        <Input placeholder="brown"/>
      </Form.Item>
      <Form.Item
        label="Tags"
        name="tags"
        rules={[{ message: 'Please give at least one tag (CSV)' }]}
      >
        <Input placeholder="fluffy, friendly"/>
      </Form.Item>
    </Form>
  );
}