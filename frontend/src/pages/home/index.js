import React, { useState, useEffect, useCallback } from 'react'
import { Wrapper, TitleWrapper} from "./index.styles"
import { Table, Tag, Modal, Form, Spin } from 'antd'
import { Typography, Divider } from 'antd'
import { Button } from 'antd'
import CatForm from "./../../components/form"
import {FormWrapper} from './index.styles'
import axios from 'axios'
const { Title, Paragraph } = Typography

const endpoint = process.env.REACT_APP_BACKEND_URL

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <a>{text}</a>,
  },
  {
    title: 'Color',
    dataIndex: 'color',
    key: 'color',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
      <>
        {tags.map(tag => {
          let colors = ['geekblue', 'green', 'volcano']
          let color = colors[parseInt(Math.random()*colors.length)]
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          )
        })}
      </>
    ),
  }
]


export default function Home(props) {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [data, setData] = useState([])
  const [formData, setFormData] = useState({})
  const [form] = Form.useForm()
  const [isLoading, setIsLoading] = useState(false)
  const [dataIsLoading, setDataIsLoading] = useState(false)

  const getData = useCallback(
    async () => {
      const resp = await axios.get(endpoint)
      return resp.data.map((v,i)=>({...v, key:i}))
    },
    [endpoint]
  )

  useEffect(() => {
    setDataIsLoading(true)
    getData().then((catData)=>{
      setData(catData)
      setDataIsLoading(false)
    })
  }, [getData])

  const addCatHandler = () => {
    setIsModalVisible(true)
  }

  const handleOk = () => {
    setIsLoading(true)

    axios.post(endpoint, formData)
    .then((response) => {
      setIsLoading(false)
      setIsModalVisible(false)
      
      // Pull new data
      setDataIsLoading(true)
      getData().then((catData)=>{
        setData(catData)
        setDataIsLoading(false)
      })
    }, (error) => {
      console.error(error)
      setIsLoading(false)
      form.resetFields()
    })
  }

  const handleCancel = () => {
    form.resetFields()
    setIsModalVisible(false)
  }

  const handleFormData = (newField) => {
    const newData = {...formData, ...newField}
    setFormData(newData)
  }

  return (
    <>
      <Wrapper>
        <Typography>
          <TitleWrapper>
            <Title>Cat-A-Log 🐈‍⬛</Title>
            <Button type="primary" onClick={addCatHandler}>Report Cat</Button>
          </TitleWrapper>
          <Paragraph>
            Neighbourhood cats roam the streets. You've been challenged by HR to report all your sightings of cats
            immediately. To help you with this chore, they have kindly provided this handy web app with which 
            you can report cats!
          </Paragraph>
          <Divider />
          <Title level={3}>Cat Sightings</Title>
          <Spin tip="Loading..." spinning={dataIsLoading}>
            <Table columns={columns} dataSource={data} />
          </Spin>
        </Typography>
      </Wrapper>
      
      <Modal title="Report Cat" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Spin tip="Loading..." spinning={isLoading}>
          <FormWrapper>
            <CatForm handleOnFinished={handleFormData} form={form}/>
          </FormWrapper>
        </Spin>
      </Modal>
    </>
  )
}

