# Cat-A-Log 🐈‍⬛

Neighbourhood cats roam the streets. You've been challenged by HR to report all your sightings of cats immediately. To help you with this chore, they have kindly provided this handy web app with which you can report cats!

[YouTube Video of it in Action!](https://youtu.be/V20luTaRksk)

## To Do

- [x] Setup Backend
- [x] Setup Frontend
- [x] Create Docker image describing Backend
- [x] Create K8S cluster per diagram
- [] Use Helm Charts (next DICE)
- [] Use Ingress (next DICE)

## Kubernetes (K8S)

Kubernetes is an abstraction on container management systems like Docker, which automatically manages images that describe microservices in a way that ensures:
- availability e.g. more uptime
- scalability e.g. effortless load balancing and replica increase
- failure recovery e.g. restarting a microservice in the event of failure

To learn more about Kubernetes including terms like Pod & Service, see: [Kubernetes 4 Hour Intensive](https://www.youtube.com/watch?v=X48VuDVv0do&t=9197s&ab_channel=TechWorldwithNana) and [Practical Guide to Kubernetes (Python)](https://www.youtube.com/watch?v=d1ZMnV4yM1U&ab_channel=ThatDevOpsGuy).


The architecture is as follows. In essence, each pod represents a microservice which are replicated within nodes for better fault tolerance, scale and up time. The microservice is described by its Docker container image, which get configured by the Deployment `.yaml` config files which are a kind of pod blueprint. Since each pod can readily die and restart, they don't have a fixed static IP which necessitates the Service component. Services exposes a pods port to pods internal to a node, and those external to the node (e.g. a frontend). With each pod, there's an associated service to let the Sanic backend and Mongo express admin dashboard communicate with the MongoDB pod, where Sanic and Mongo are made externally accessible using External Services. Nodes contain pods, and within each cluster you can have many node replicas for more scale and robustness.

![GitHub Logo](https://i.imgur.com/kayd70j.png)

## Docker

Docker is a container environment that enables developers to describe the environment and confirguation of some entity (say a microservice, like a Sanic backend) to increase its portability. That way anyone with the image can have the same experience as on the developers machine, as all its dependencies and necessary config are wrapped up with it in the image. 
\
### dockerfile 

To include our Sanic backend as a pod, we had to make a docker image for it. This involved specifying the Docker image linux environment where we will "host" our image, in this case `sanicframework/sanic:LTS` and the steps needed to move our Python backend into the linux system and install its dependencies. This happens in the dockerfile.

### docker-compose

Docker-compose is a way of composing services together by specifying images along with their configurations. Without going into too much detail, we used this to give our image some extra config, and a name.

## How To Build

This assumes you have a mac (or similar unix environment). The setup for Windows is slightly different. Navigate into the `./backend` folder. 

### Pre-requisites
We need `hyperkit` to enable hypervisor virtualisation, in which your Nodes/Pods will live as Virtual Machines. `minikube` as a way to locally host a K8S cluster on your PC (no need for servers). `kubectl` as a way of interacting with K8S clusters via the CLI (e.g. it lets you add Pods, view logs etc).

```
brew install hyperkit
brew install minikube
brew install kubectl
```

### Start Minikube

```
minikube start --vm-driver=hyperkit
```
Will start the K8S hosting environment (Minikube). We will later configure it with Kubectl to setup our K8S cluster as defined by our config files!

### Build and Push the sanic backend Docker image

Once signed into your Docker account, and you've changed the image to point to your account:
```
docker-compose build python && docker-compose push python
```
This will build the image and then push it out to your account, where it's publically hosted. This must be done each time you make a change to `server.py` which you want to see appear in your K8S cluster!

### Launch the Pods and their Services

Each `.yaml` file has the Deployment (pod blueprint) at the top, followed by its corresponding Service as a document in the same file (i.e. after the `---`). We use `kubectl` as a way to configure our K8S cluster.

```
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongodb-secret.yaml
kubectl apply -f mongodb.yaml
kubectl apply -f sanic.yaml
kubectl apply -f mongo-express.yaml
```

We must setup the Configmap and Secret components first, because the following Deployments reference key-values that are stored in these files. Configmaps are just key-value stores for values we use in many `.yaml`, and Secret's are a way of storing Sensitive info like passwords in a way that sharing the it won't compromise security!

### Launch the Backend

To make our external services accessible, we run:

```
minikube service mongodb-express-service
minikube service sanic-backend-service
```

They will launch a browser window with URL that connects to the service. Change the URL in the `.env` react file to that seen in the browser for `sanic-backend-service`, that way the frontend will hit your locally hosted Sanic pod.

### Launch the Frontend

Navigate to the Frontend.

```
yarn start
```

This will open a new browser window with the React app! YOU DID IT! Now report those cats.

