from sanic import Sanic
from sanic.response import json as sanic_json_response
from sanic.exceptions import NotFound
from sanic_motor import BaseModel
from bson import json_util, ObjectId
import json
from sanic_motor import BaseModel
import os
from sanic_cors import CORS, cross_origin

# custom json response to handle ObjectId
def json_response(data):
    parsed = json.loads(json_util.dumps(data))

    return sanic_json_response(parsed)

# Configure the App
app = Sanic(__name__)
CORS(app)

def settings():
    u = os.environ["MONGODB_ADMINUSERNAME"]
    p = os.environ["MONGODB_ADMINPASSWORD"]
    url = os.environ["MONGODB_SERVER"]
    db = "cats"
    motor_uri =  f"mongodb://{u}:{p}@{url}/{db}?authSource=admin"
    return dict(
        MOTOR_URI=motor_uri,
        LOGO=None,
    )
    
app.config.update(settings())
BaseModel.init_app(app)

# Define DB Model for Cat collection
class Cat(BaseModel):
    __coll__ = "cats"

# Add new cat
@app.route("/", methods=["POST"])
async def create_cat(request):
    cat = request.json
    cat["_id"] = str(ObjectId())
    new_cat = await Cat.insert_one(cat)
    created_cat = await Cat.find_one(
        {"_id": new_cat.inserted_id}, as_raw=True
    )
    return json_response(created_cat)

# List all cats
@app.route("/", methods=["GET", "OPTIONS"])
async def get_cats(request):
    cats = await Cat.find(as_raw=True)
    return json_response(cats.objects)

# Run our app
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
